##Wifi Manager
This folder include wifi manager binding and GUI.
Only support Station mode.

##Function
- Wifi ON and OFF
- Wifi scan and display result
- Connect to open and secured AccessPoint
- Disconnect connection

##Guidance for building Wi-Fi manager Binding
A. Prepare sdk environment
   - bitbake below:
     $ bitbake agl-demo-platform-crosssdk
   - install sdk
     $ build/tmp/deploy/sdk/poky-agl-glibc*.sh
B. Build
   - Go to folder where WiFi Bindings source code exists
   - run below command for setup and make
     $ . /opt/poky-agl/xxx/environment-setup-cortexa15hf-vfp-neon-poky-linux-gnueabi
     $ mkdir build
     $ cd build
     $ cmake ..
     $ make
     <widget wll be generated>
C. Running app
   - copy widget to target board
   - do install and start the app
     # afm-util install *.wgt
     # afm-util start <apps's name>@0.1

##TODO
- ADD token security
- ADD event receive

